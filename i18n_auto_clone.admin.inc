<?php
/**
 * @file
 * Administration functions for i18n_auto_clone.
 */

/**
 * Administration form for setting defaults.
 */
function i18n_auto_clone_defaults_settings_form(&$form_state) {
  $form['i18n_auto_clone_node_types'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#title' => t('Content types to keep dummy translations for'),
    '#options' => array(),
    '#default_value' => variable_get('i18n_auto_clone_node_types', array()),
    '#description' => t('The content types selected will have translations generated by a cloning procedure. These nodes will be marked as needing a translation update. When changes are made to the default translation in the set, the dummy clones will be updated.'),
    '#size' => 15,
  );
  foreach (node_get_types('names') as $type => $name) {
    $form['i18n_auto_clone_node_types']['#options'][$type] = check_plain($name);
  }
  $languages = locale_language_list('name', TRUE);
  // Unset the language set as default.
  unset($languages[variable_get('i18n_auto_clone_default_language', 'en')]);

  $form['i18n_auto_clone_enabled_languages'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#title' => t('Languages to enable for dummy translations'),
    '#options' => $languages,
    '#default_value' => variable_get('i18n_auto_clone_enabled_languages', array()),
    '#description' => t('The content types selected will have translations generated by a cloning procedure. These nodes will be marked as needing a translation update. When changes are made to the default translation in the set, the dummy clones will be updated.'),
    '#size' => 15,
  );
  $form['i18n_auto_clone_default_language'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Default language'),
    '#default_value' => variable_get('i18n_auto_clone_default_language', 'en'),
    '#options' => locale_language_list('name', TRUE),
    '#description' => t('Choose the default language that will act as the master node for the translation clone set, sitewide. Creating a new node (of an enabled type) in this language will set it as the "master node" and new clones will be created for all enabled languages.'),
  );
  $form['i18n_auto_clone_processing'] = array(
    '#type' => 'radios',
    '#title' => t('Clone processing'),
    '#default_value' => variable_get('i18n_auto_clone_processing', 'during_cron'),
    '#options' => array(
      'during_cron' => t('During cron'),
      'immediately' => t('Immediately upon node save'),
    ),
  );
  $form['i18n_auto_clone_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron processing limit'),
    '#default_value' => variable_get('i18n_auto_clone_cron_limit', 10),
    '#size' => 20,
    '#required' => TRUE,
  );
  $form['i18n_auto_clone_batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Cloning batch size'),
    '#default_value' => variable_get('i18n_auto_clone_batch_size', 10),
    '#size' => 20,
    '#required' => TRUE,
    '#description' => t('Number of clones to queue and/or create per batch cycle. Increasing this value may speed up the batch process at the expense of server resources, and may result in fatal errors if the PHP memory limit is not high enough. The queueing process is much less intensive, so this number can be very high for that purpose.'),
  );
  $form['#submit'][] = 'i18n_auto_clone_defaults_settings_form_submit';
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#theme'] = 'system_settings_form';
  return system_settings_form($form);
}

/**
 * Form submit handler. Orphans existing clones if necessary.
 */
function i18n_auto_clone_defaults_settings_form_submit(&$form, &$form_state) {
  $types = $form['i18n_auto_clone_node_types']['#default_value'];
  $new_types = $form_state['values']['i18n_auto_clone_node_types'];
  foreach ($types as $key => $value) {
    // This type was previously set to be cloned.
    if ($value) {
      // This type has now been set to not be cloned.
      if ($new_types[$key] == FALSE) {
        // Remove all current records of clones of this type, abandoning them.
        db_query("DELETE ac.* FROM {i18n_auto_clone} ac LEFT JOIN {node} n ON ac.tnid = n.nid WHERE n.type = '%s'", $key);
      }
    }
  }
  $languages = $form['i18n_auto_clone_enabled_languages']['#default_value'];
  $new_languages = $form_state['values']['i18n_auto_clone_enabled_languages'];
  foreach ($languages as $key => $value) {
    // This language was previously set to be cloned.
    if ($value) {
      // This language has now been set to not be cloned.
      if ($new_languages[$key] == FALSE) {
        // Remove all current records of clones of this language, abandoning them.
        db_query("DELETE FROM {i18n_auto_clone} WHERE language = '%s'", $key);
      }
    }
  }
}


/**
 * Form callback. Batch process utility.
 */
function i18n_auto_clone_batch_form() {
  $immediate_processing = variable_get('i18n_auto_clone_processing', 'during_cron') == 'immediately' ? TRUE : FALSE;
  $types = array();
  $enabled_types = variable_get('i18n_auto_clone_node_types', array());
  foreach (node_get_types('names') as $type => $name) {
    if (isset($enabled_types[$type]) && $enabled_types[$type]) {
      $types[$type] = check_plain($name);
    }
  }
  $form['i18n_auto_clone_batch_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type to batch process'),
    '#options' => $types,
    '#default_value' => '',
    '#multiple' => FALSE,
    '#required' => TRUE,
    '#description' => t('The content types selected will have translations generated by a cloning procedure. These nodes will be marked as needing a translation update. When changes are made to the default translation in the set, the dummy clones will be updated. Note that the node types will not be available here unless they are enabled on the settings overview page.'),
  );
  $batch_languages = array();
  $languages = locale_language_list('name', TRUE);
  $enabled_languages = variable_get('i18n_auto_clone_enabled_languages', array());
  foreach ($languages as $type => $name) {
    if ($enabled_languages[$type]) {
      $batch_languages[$type] = check_plain($name);
    }
  }
  $form['i18n_auto_clone_batch_language'] = array(
    '#type' => 'select',
    '#title' => t('Language to batch process'),
    '#options' => $batch_languages,
    '#multiple' => FALSE,
    '#default_value' => '',
    '#required' => TRUE,
    '#description' => t('The language selected will have translations generated by a cloning procedure. These nodes will be marked as needing a translation update. When changes are made to the default translation in the set, the dummy clones will be updated. Note that the languages will not be available here unless they are enabled on the settings overview page.'),
  );
  $form['queue'] = array(
    '#type' => 'submit',
    '#value' => t('Queue nodes for cloning'),
    '#suffix' => '<p>' . t('This will queue all the missing tranlations for the selected node type.') . '</p>',
  );
  $form['clone'] = array(
    '#type' => 'submit',
    '#value' => t('Create or update clones'),
    '#suffix' => '<p>' . t('This will create or update all clones currently queued for cloning, regardless of the node type and language options selected above.') . '</p>',
  );
  $immediate_title = t('Queue nodes for cloning and clone them');

  if ($immediate_processing) {
    $form['queue']['#value'] = t('Queue nodes for cloning and clone them');
    $form['queue']['#suffix'] = '<p>' . t('This will queue all the missing tranlations for the selected node type, and execute the cloning. The two processes are combined because immediate processing is selected on the Overview configuration page.') . '</p>';
  }

  return $form;
}

// @TODO: Form validation. (they need to select at least one of each option.
/**
 * Batch form submit handler function.
 *
 * Sets up batch processeses based on the submit button pressed.
 */
function i18n_auto_clone_batch_form_submit($form, &$form_state) {
  $options = array(
    'node_type' => $form_state['values']['i18n_auto_clone_batch_node_type'],
    'language' => $form_state['values']['i18n_auto_clone_batch_language'],
    'operation' => $form_state['values']['op'],
  );
  if ($form_state['values']['op'] == t('Queue nodes for cloning') || $form_state['values']['op'] == t('Queue nodes for cloning and clone them')) {
    $batch = array(
      'title' => $form_state['values']['op'],
      'operations' => array(
        array(
          'i18n_auto_clone_batch_queue',
          array($options),
        ),
      ),
      'finished' => 'i18n_auto_clone_batch_finished',
      'file' => drupal_get_path('module', 'i18n_auto_clone') . '/i18n_auto_clone.admin.inc',
    );
  }
  else {
    $batch = array(
      'title' => $form_state['values']['op'],
      'operations' => array(
        array(
          'i18n_auto_clone_batch_clone',
          array($options),
        ),
      ),
      'finished' => 'i18n_auto_clone_batch_finished',
      'file' => drupal_get_path('module', 'i18n_auto_clone') . '/i18n_auto_clone.admin.inc',
    );
  }
  batch_set($batch);
}

/**
 * Batch function to add nodes to the queue.
 */
function i18n_auto_clone_batch_queue($options, &$context) {
  // We'll need to load this include file.
  module_load_include('inc', 'node', 'node.pages');
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_result'] = 0;
  }
  else {
    // Keep track of previous cycle's progress so we don't get stuck in a loop.
    $context['sandbox']['previous_progress'] = $context['sandbox']['progress'];
  }
  // We need a query to count number of rows.
  if (!isset($context['sandbox']['max'])) {
    $context['sandbox']['max'] = _i18n_auto_clone_batch_queue_number_left($options['node_type'], $options['language']);
  }

  // Loop through x number of rows at a time.
  $batch_size = variable_get('i18n_auto_clone_batch_size', 10);
  $default_language = variable_get('i18n_auto_clone_default_language', 'en');


  // If set to clone and queue, we do not set the $from parameter for
  // db_query_range, as the newly created clones cause the query result set to
  // progressively decline in number, which would result in only half of the
  // clones being created.
  $immediate = variable_get('i18n_auto_clone_processing', 'during_cron') == 'immediately' ? TRUE : FALSE;
  $node_type = $options['node_type'];
  $language = $options['language'];

  $result = db_query_range("SELECT n.nid FROM {node} n
  WHERE n.type = '%s'
  AND n.language = '%s'
  AND n.nid NOT IN(
    # These are tnids from translations that do not have clone records.
    SELECT n.tnid FROM {node} n
    LEFT JOIN {i18n_auto_clone} clone ON n.nid = clone.nid
    WHERE n.language = '%s'
    AND n.type = '%s'
    AND clone.tnid IS NULL
  )
  AND n.nid NOT IN(
    # These are tnids of nodes that have clone records.
    SELECT n.tnid FROM {node} n
    LEFT JOIN {i18n_auto_clone} clone ON n.nid = clone.nid
    WHERE n.language = '%s'
    AND n.type = '%s'
    AND clone.tnid IS NOT NULL
  )
  AND n.nid NOT IN(
    # These are tnid values from clone records that have not yet been cloned.
    SELECT clone.tnid FROM {i18n_auto_clone} clone
    LEFT JOIN {node} n ON clone.tnid = n.nid
    WHERE clone.language = '%s'
    AND n.type = '%s'
    AND clone.nid = 0
  )
  ORDER BY n.nid", array($node_type, $default_language, $language, $node_type, $language, $node_type, $language, $node_type), 0, $batch_size);
  $count = 0;
  $nids = array();
  while ($row = db_fetch_object($result)) {
    $nids[] = $row->nid;
    $count ++;
  }
  $op = $options['operation'] == t('Queue nodes for cloning') ? 'queued' : 'queued and cloned';
  foreach ($nids as $nid) {
    $node = node_load($nid);
    $result = i18n_auto_clone_queue_clone($node, $options['language']);
    if ($result && $result != 'duplicate' && $result != 'aborted') {
      if ($op == 'queued') {
        $context['results'][] = t('%language clone for nid:%nid successfully queued.', array('%language' => $options['language'], '%nid' => $nid));
      }
      else {
        $context['results'][] = t('%language clone for nid:%nid successfully queued and cloned. Clone nid: %clone', array(
            '%language' => $options['language'],
            '%nid' => $nid,
            '%clone' => $result,
          ));
      }
    }
  }
  // Update our progress information.
  $context['sandbox']['progress'] += $count;
  // Add a faux-count to the progress if we're stuck in a loop.
  if ($context['sandbox']['previous_progress'] === $context['sandbox']['progress']) {
    $context['sandbox']['progress']++;
  }
  $context['message'] = t('Now processing %current of %max nodes to be potentially added to the cloning queue.', array(
    '%current'=> $context['sandbox']['progress'],
    '%max' => $context['sandbox']['max'],
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

/**
 * Query to find number of nodes left to add to the queue.
 */
function _i18n_auto_clone_batch_queue_number_left($node_type, $language) {
  $default_language = variable_get('i18n_auto_clone_default_language', 'en');
  $number_to_process = db_result(db_query("SELECT count(n.nid) FROM {node} n
  WHERE n.type = '%s'
  AND n.language = '%s'
  AND n.nid NOT IN(
    # These are tnids from translations that do not have clone records.
    SELECT n.tnid FROM {node} n
    LEFT JOIN {i18n_auto_clone} clone ON n.nid = clone.nid
    WHERE n.language = '%s'
    AND n.type = '%s'
    AND clone.tnid IS NULL
  )
  AND n.nid NOT IN(
    # These are tnids of nodes that have clone records.
    # (Redundant, but better for documentation.)
    SELECT n.tnid FROM {node} n
    LEFT JOIN {i18n_auto_clone} clone ON n.nid = clone.nid
    WHERE n.language = '%s'
    AND n.type = '%s'
    AND clone.tnid IS NOT NULL
  )
  AND n.nid NOT IN(
    # These are tnid values from clone records that have not yet been cloned.
    SELECT clone.tnid FROM {i18n_auto_clone} clone
    LEFT JOIN {node} n ON clone.tnid = n.nid
    WHERE clone.language = '%s'
    AND n.type = '%s'
    AND clone.nid = 0
  )
  ORDER BY n.nid", array($node_type, $default_language, $language, $node_type, $language, $node_type, $language, $node_type)));

  return $number_to_process;
}
/**
 * Batch function to clone nodes already in the queue.
 */
function i18n_auto_clone_batch_clone($options, &$context) {
  // We'll need to load this include file.
  module_load_include('inc', 'node', 'node.pages');

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_result'] = 0;
  }
  else {
    // Keep track of previous cycle's progress so we don't get stuck in a loop.
    $context['sandbox']['previous_progress'] = $context['sandbox']['progress'];
  }

  // We query to count number of rows and set it on the first cycle.
  if (!isset($context['sandbox']['max'])) {
    $context['sandbox']['max'] = _i18n_auto_clone_batch_clone_number_left();
  }

  // Loop through a variable number of rows per batch cycle.
  $batch_size = variable_get('i18n_auto_clone_batch_size', 10);
  $result = db_query_range("SELECT clone.nid, clone.tnid, clone.language FROM {i18n_auto_clone} clone
    LEFT JOIN {node} n ON clone.tnid = n.nid
    WHERE clone.needs_update = 1 ORDER BY clone.timestamp ASC", 0, $batch_size);
  $count = 0;
  $clones = array();
  while ($clone = db_fetch_object($result)) {
    $original_node = node_load($clone->tnid);
    $clones[] = array(
      'original_node' => $original_node,
      'language' => $clone->language,
      'clone_nid' => $clone->nid,
    );
    $count++;
  }
  if (!empty($clones)) {
    // Set a flag so our nodeapi operations are ignored.
    global $i18n_auto_clone;
    $i18n_auto_clone = TRUE;
    foreach ($clones as $clone) {
      if ($results = i18n_auto_clone_create_clone($clone['original_node'], $clone['language'], $clone['clone_nid'])) {
        $context['results'][] = t('%language clone processed with the error status or clone nid of: %results', array('%language' => $language, '%results' => $results));
      }
    }
    $i18n_auto_clone = FALSE;
  }
  // Update our progress information.
  $context['sandbox']['progress'] += $count;
  // Add a faux-count to the progress if we're stuck in a loop.
  if ($context['sandbox']['previous_progress'] === $context['sandbox']['progress']) {
    $context['sandbox']['progress'] ++;
  }
  $context['message'] = t('Now creating %current of %max clones.', array(
    '%current'=> $context['sandbox']['progress'],
    '%max' => $context['sandbox']['max'],
  ));

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Query to find number of entries in the queue left to clone.
 */
function _i18n_auto_clone_batch_clone_number_left() {
  // Join on node's nid column, since clones may not yet exist in the node table.
  $result = db_result(db_query("SELECT COUNT(clone.tnid) FROM {i18n_auto_clone} clone
    LEFT JOIN {node} n ON clone.tnid = n.nid
    WHERE clone.needs_update = 1"
  ));
  return $result;
}

/**
 * End of batch function.
 */
function i18n_auto_clone_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) . ' items processed.';
    $message .= theme('item_list', $results);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);

    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    watchdog('i18_auto_clone', 'i18_auto_clone encountered an error during batch batch. The operation name is %error_operation and arguments are @arguments.', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1]),
      ), WATCHDOG_ERROR);
  }

  drupal_set_message($message);
}
