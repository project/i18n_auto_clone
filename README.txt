OVERVIEW
--------

This module creates an administration interface to select certain node types
that will have translations cloned for them automatically. This addresses issues
where nodes that are not translated will not be available in those languages,
even though they might be relevant or necessary. Any node that has been generated
automatically has a "dummy" flag assigned to it, which allows it to remain
syncronized to its default translation.

A batch process is provided to make sure all nodes can be updated to fill in any
missing translations with a clone of the default translation (where tnid == nid).

Link and nodereference fields are checked during cloning, and pointed to
appropriate translated nodes if they exist.

KNOWN ISSUES
------------
Due to the nature of the process, certain race conditions can affect the way
link and nodereference fields are re-pointed in cloned nodes. Marking existing
clones as needing updates in the i18n_auto_clone table might fix this.

If Pathauto is used to create the path for the source node, and the option is
selected to create clones immediately upon save, the clones will not immediately
inherit the path of the source. They will only inherit the path upon re-save of
the source.

Development sponsored by Tektronix.
